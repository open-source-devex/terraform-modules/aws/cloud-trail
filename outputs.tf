output "trail_name" {
  value = local.trail_name
}

output "trail_s3_bucket" {
  value = local.trail_bucket_name
}

output "trail_cloudwatch_log_group_name" {
  value = local.trail_log_group_name
}
